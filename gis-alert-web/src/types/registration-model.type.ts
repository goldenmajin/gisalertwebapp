export class RegistrationModel {
	userName: string;
	password: string;
	email: string;
	roleId: string;
}
