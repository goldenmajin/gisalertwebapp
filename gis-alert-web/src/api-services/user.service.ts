import { environment } from './../environments/environment.prod';
import { LoginModel } from '@apiTypes/login-model.type';
import { Injectable } from '@angular/core';
import { RegistrationModel } from 'src/types/registration-model.type';
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root',
})
export class UserService {
	constructor(private http: HttpClient) {}
	register(body: RegistrationModel) {
		return this.http.post<any>(
			`${environment.baseUrl}/api/User/register`,
			body
		);
	}

	login(body: LoginModel) {
		return this.http.post<any>(`${environment.baseUrl}/api/User/login`, body);
	}

	getRoles() {
		return this.http.get<any>(`${environment.baseUrl}/api/User/roles`);
	}

	changePassword() {}

	getCurrentUserRole() {
		const token = localStorage.getItem('token');
		if (token) {
			var payLoad = JSON.parse(window.atob(token.split('.')[1]));
			return payLoad.role;
		}
		return null;
	}
}
