import { Injectable } from '@angular/core';
import Feature from 'ol/Feature';
import Circle from 'ol/geom/Circle';
import TileLayer from 'ol/layer/Tile';
import Map from 'ol/Map';
import { fromLonLat, getPointResolution, METERS_PER_UNIT } from 'ol/proj';
import OSM from 'ol/source/OSM';
import View from 'ol/View';
import { Fill, Icon, Stroke, Style } from 'ol/style';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Point from 'ol/geom/Point';

@Injectable({
	providedIn: 'root',
})
export class MappingService {
	constructor() {}

	public createMap(
		longitude: number,
		latitude: number,
		zoom: number,
		elementId: string
	): Map {
		return new Map({
			view: new View({
				center: fromLonLat([longitude, latitude]),
				zoom: zoom,
			}),
			layers: [
				new TileLayer({
					properties: { name: 'baseLayer' },
					source: new OSM(),
					zIndex: 0,
				}),
			],
			target: elementId,
		});
	}

	public recenterMap(map: Map, location: any, zoom: number) {
		map.setView(
			new View({
				center: fromLonLat([location.lon, location.lat]),
				zoom: zoom,
			})
		);
	}

	public renderFeatures(map: Map, locations: any[]) {
		const circleFeatures = locations.map((location) =>
			this.renderCircularArea(
				{
					lon: location.longitude,
					lat: location.latitude,
					radius: location.radius,
					color: location.type.color,
				},
				this.getResolutionFactor(map)
			)
		);
		const markers = locations.map((location) => this.renderMarker(location));

		map.addLayer(
			new VectorLayer({
				source: new VectorSource({
					features: circleFeatures,
				}),
				zIndex: 1,
			})
		);

		map.addLayer(
			new VectorLayer({
				source: new VectorSource({
					features: markers,
				}),
				zIndex: 2,
			})
		);
	}

	renderMarker(location: any): Feature<any> {
		var markerFeature = new Feature({
			geometry: new Point(fromLonLat([location.longitude, location.latitude])),
			type: 'marker',
			data: location,
		});

		var markerStyle = this.getMarkerStyle(location.type.color);
		markerFeature.setStyle(markerStyle);

		return markerFeature;
	}

	renderCircularArea(location: any, resolutionFactor: number): Feature<any> {
		const circleFeature = new Feature({
			geometry: new Circle(
				fromLonLat([location.lon, location.lat]),
				(location.radius / METERS_PER_UNIT['m']) * resolutionFactor
			),
		});

		var style = this.getCircularAreaStyle(location.color);
		circleFeature.setStyle(style);
		return circleFeature;
	}

	getCircularAreaStyle(color: string): Style {
		return new Style({
			stroke: new Stroke({
				color: '#' + `${color}`,
				width: 3,
			}),
			fill: new Fill({
				color: [51, 51, 51, 0.3],
			}),
		});
	}

	getMarkerStyle(color: string): Style {
		return new Style({
			image: new Icon({
				anchor: [0.5, 0.9],
				color: '#' + `${color}`,
				crossOrigin: 'anonymous',
				src: 'assets/icons/room_white_24dp.svg',
				scale: 1.5,
			}),
		});
	}

	resetMap(map: Map) {
		const baseMapLayer =
			map
				.getLayers()
				.getArray()
				.find((layer) => layer.getProperties()['name'] === 'baseLayer') ||
			new TileLayer({
				source: new OSM(),
			});

		map.setLayers([baseMapLayer]);
	}

	getResolutionFactor(map: Map): number {
		const view = map.getView();
		const viewResolution = view.getResolution() ?? 1;
		const center = view.getCenter() ?? [0, 0];
		const pointResolution = getPointResolution(
			'EPSG:3857',
			viewResolution,
			center,
			'm'
		);
		return viewResolution / pointResolution;
	}
}
