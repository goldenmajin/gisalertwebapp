import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';

@Injectable({
	providedIn: 'root',
})
export class StatisticsService {
	constructor(private httpService: HttpClient) {}

	public getEventTypeStatistic(searchOptions: any): Observable<any> {
		searchOptions.dateFrom = searchOptions.dateFrom
			? searchOptions.dateFrom.toISOString()
			: '';
		searchOptions.dateTo = searchOptions.dateTo
			? searchOptions.dateTo.toISOString()
			: '';

		return this.httpService.get<any>(
			`${environment.baseUrl}/api/statistics/event/by-type`,
			{ params: searchOptions }
		);
	}

	public getUsersStatistic() {
		return this.httpService.get<any>(
			`${environment.baseUrl}/api/statistics/users/by-roles`
		);
	}
}
