import { environment } from './../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class EventService {
	constructor(private httpService: HttpClient) {}

	public getData(): Observable<any> {
		return this.httpService.get<any>('assets/data/mock-data.json');
	}

	public get(id: number): Observable<any> {
		return this.httpService.get<any>(`${environment.baseUrl}/api/Event/${id}`);
	}

	public getAll(): Observable<any> {
		return this.httpService.get<any>(`${environment.baseUrl}/api/Event`);
	}

	public delete(id: number): Observable<any> {
		return this.httpService.patch<any>(
			`${environment.baseUrl}/api/Event/delete/${id}`,
			null
		);
	}

	public getEventTypes(): Observable<any> {
		return this.httpService.get<any>(`${environment.baseUrl}/api/Event/types`);
	}

	public uploadImages(files: any): Observable<any> {
		const formData = new FormData();
		files.forEach((file: any) => {
			formData.append('files', file);
		});

		return this.httpService.post<any>(
			`${environment.baseUrl}/api/Event/uploadImages`,
			formData
		);
	}

	public add(add: any): Observable<any> {
		const formData = new FormData();
		add.files.forEach((file: any) => {
			formData.append('files', file);
		});
		delete add.files;
		const blobOverrides = JSON.stringify(add);
		formData.append('add', blobOverrides);
		return this.httpService.post<any>(
			`${environment.baseUrl}/api/Event/add`,
			formData
		);
	}

	public edit(id: number, detail: any): Observable<any> {
		return this.httpService.put<any>(
			`${environment.baseUrl}/api/Event/edit/${id}`,
			detail
		);
	}

	public searchLocation(searchText: string): Observable<any> {
		return this.httpService.get(
			`https://nominatim.openstreetmap.org/search?q=${searchText}&format=json`
		);
	}
}
