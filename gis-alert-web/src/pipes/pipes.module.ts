import { EllipsisPipe } from './ellipsis.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
	declarations: [EllipsisPipe],
	imports: [CommonModule],
	exports: [EllipsisPipe],
})
export class PipesModule {}
