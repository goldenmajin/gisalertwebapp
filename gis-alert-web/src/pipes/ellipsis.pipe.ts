import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'ellipsis',
})
export class EllipsisPipe implements PipeTransform {
	transform(value: string, allowedLength: number): string {
		return value.length > allowedLength
			? value.substring(0, allowedLength) + '...'
			: value;
	}
}
