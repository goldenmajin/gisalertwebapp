import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { NgxMatDatetimePickerModule } from '@angular-material-components/datetime-picker';
import {
	NgxMatMomentModule,
	NgxMomentDateModule,
} from '@angular-material-components/moment-adapter';

@NgModule({
	declarations: [],
	imports: [
		CommonModule,
		MatToolbarModule,
		MatSidenavModule,
		MatListModule,
		MatIconModule,
		MatButtonModule,
		MatInputModule,
		MatTooltipModule,
		MatCardModule,
		MatDialogModule,
		MatTabsModule,
		MatFormFieldModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatAutocompleteModule,
		MatSnackBarModule,
		MatSelectModule,
		MatMenuModule,
		NgxMatDatetimePickerModule,
		NgxMatMomentModule,
	],
	exports: [
		MatToolbarModule,
		MatSidenavModule,
		MatListModule,
		MatIconModule,
		MatButtonModule,
		MatInputModule,
		MatTooltipModule,
		MatCardModule,
		MatDialogModule,
		MatTabsModule,
		MatFormFieldModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatAutocompleteModule,
		MatSnackBarModule,
		MatSelectModule,
		MatMenuModule,
		NgxMatDatetimePickerModule,
		NgxMatMomentModule,
	],
})
export class MaterialModule {}
