import { ImageDialogComponent } from './image-dialog/image-dialog.component';
import { PipesModule } from './../../pipes/pipes.module';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { MaterialModule } from './../material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ControlErrorComponent } from './control-error/control-error.component';

@NgModule({
	declarations: [
		FileUploadComponent,
		ToolbarComponent,
		SearchBarComponent,
		ImageDialogComponent,
		ControlErrorComponent,
	],
	imports: [
		CommonModule,
		MaterialModule,
		FormsModule,
		PipesModule,
		ReactiveFormsModule,
		NgbModule,
	],
	exports: [
		FileUploadComponent,
		ToolbarComponent,
		SearchBarComponent,
		ImageDialogComponent,
		ControlErrorComponent,
	],
})
export class CustomUiComponentsModule {}
