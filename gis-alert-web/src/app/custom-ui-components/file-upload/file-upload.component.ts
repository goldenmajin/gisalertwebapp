import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { concatMap, Subject } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { Observer } from 'rxjs/internal/types';

@Component({
	selector: 'app-file-upload',
	templateUrl: './file-upload.component.html',
	styleUrls: ['./file-upload.component.scss'],
})
export class FileUploadComponent implements OnInit {
	@Input() formGroup: FormGroup;
	@Input() controlName: string;
	@Input() multiple: boolean;
	@Input() acceptedFormats: string[];
	@Input() maxFilesNumber: number;
	@Input() canUpload: boolean = false;
	@Input() eventImages: string[];
	@Output() thumbnailClick: EventEmitter<string> = new EventEmitter<string>();
	public uploadToThumbnailsSubject: Subject<File> = new Subject<File>();
	public thumbnailImages: string[] = [];

	constructor() {}

	ngOnInit(): void {
		if (!!this.eventImages) {
			this.thumbnailImages = this.eventImages;
		}
		this.uploadToThumbnailsSubject
			.pipe(concatMap((file) => this.getImageUrl(file)))
			.subscribe((url) => this.thumbnailImages.push(url));
	}

	get filesArray() {
		return this.formGroup.get('files') as FormArray;
	}

	getImageUrl(file: File): Observable<string> {
		const fileReader = new FileReader();
		return new Observable((obs: Observer<string>) => {
			fileReader.readAsDataURL(file);
			fileReader.onload = (_event) => {
				const result = fileReader.result as string;
				obs.next(result);
				obs.complete();
			};
		});
	}

	onThumbnailClick(image: string) {
		this.thumbnailClick.emit(image);
	}

	onChange(event: any) {
		let files = Array.from(event.target.files);
		if (this.filesArray.length + files.length > this.maxFilesNumber) return;
		files.forEach((file: any) => {
			if (
				!this.filesArray.controls.some(
					(control) => control.value.name === file.name
				)
			) {
				this.filesArray.push(new FormControl(file));
				this.uploadToThumbnailsSubject.next(file);
			}
		});
	}

	removeFile(index: number) {
		this.filesArray.removeAt(index);
		this.thumbnailImages.splice(index, 1);
	}
}
