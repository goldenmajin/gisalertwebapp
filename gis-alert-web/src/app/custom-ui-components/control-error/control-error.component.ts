import { ControlValidationErrorMessages } from './types/control-validation-error-messages';
import { AbstractControl, FormGroupDirective } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';
import { filter } from 'rxjs/internal/operators/filter';
import { ControlValidationErrors } from './enum/control-validation-errors.enum';

@Component({
	selector: 'app-control-error',
	templateUrl: './control-error.component.html',
})
export class ControlErrorComponent implements OnInit {
	@Input() controlName: string;
	public errorMessage: string;
	public control: AbstractControl | null;

	constructor(private formGroupDirective: FormGroupDirective) {}

	ngOnInit(): void {
		this.control = this.formGroupDirective.control.get(this.controlName);
		this.control?.valueChanges
			.pipe(
				filter(
					() =>
						!!this.control &&
						!this.control.valid &&
						(this.control.dirty || this.control.touched)
				)
			)
			.subscribe(() => this.validateControl());
	}

	validateControl() {
		if (!!!this.control?.errors) return;

		const validatorError = Object.keys(this.control.errors)[0];
		switch (validatorError) {
			case ControlValidationErrors.required: {
				this.validateFieldRequired();
				break;
			}
			case ControlValidationErrors.min: {
				this.validateFieldMin(this.control.errors[validatorError]);
				break;
			}
			case ControlValidationErrors.max: {
				this.validateFieldMax(this.control.errors[validatorError]);
				break;
			}
			case ControlValidationErrors.maxlength: {
				this.validateFieldMaxLength(this.control.errors[validatorError]);
				break;
			}
			case ControlValidationErrors.minlength: {
				this.validateFieldMinLength(this.control.errors[validatorError]);
			}
		}
	}

	validateFieldRequired() {
		this.errorMessage =
			ControlValidationErrorMessages[ControlValidationErrors.required](null);
	}

	validateFieldMin(validatorError: any) {
		this.errorMessage = ControlValidationErrorMessages[
			ControlValidationErrors.min
		](validatorError.min);
	}

	validateFieldMax(validatorError: any) {
		this.errorMessage = ControlValidationErrorMessages[
			ControlValidationErrors.max
		](validatorError.max);
	}

	validateFieldMinLength(validationError: any) {
		this.errorMessage = ControlValidationErrorMessages[
			ControlValidationErrors.minlength
		](validationError.requiredLength);
	}

	validateFieldMaxLength(validationError: any) {
		this.errorMessage = ControlValidationErrorMessages[
			ControlValidationErrors.maxlength
		](validationError.requiredLength);
	}
}
