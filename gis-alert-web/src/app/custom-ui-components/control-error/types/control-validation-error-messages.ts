import { ControlValidationErrors } from '../enum/control-validation-errors.enum';

export const ControlValidationErrorMessages: {
	[key: string]: (value: number | null) => string;
} = {
	[ControlValidationErrors.required]: () => 'Field is required',
	[ControlValidationErrors.min]: (value: number | null) =>
		`Value should be greater than ${value}`,
	[ControlValidationErrors.max]: (value: number | null) =>
		`Value should be smaller than ${value}`,
	[ControlValidationErrors.minlength]: (minLength: number | null) =>
		`Field should contain at least ${minLength} characters`,
	[ControlValidationErrors.maxlength]: (maxLength: number | null) =>
		`Field should not exceed ${maxLength} characters`,
};
