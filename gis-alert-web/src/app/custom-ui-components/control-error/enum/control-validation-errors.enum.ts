export enum ControlValidationErrors {
	required = 'required',
	min = 'min',
	max = 'max',
	minlength = 'minlength',
	maxlength = 'maxlength',
}
