import { FormBuilder } from '@angular/forms';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { debounceTime, Observable, switchMap } from 'rxjs';
import { Subject } from 'rxjs/internal/Subject';

@Component({
	selector: 'app-search-bar',
	templateUrl: './search-bar.component.html',
	styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit {
	@Input()
	searchCallback: (searchText: string) => Observable<any>;
	@Output()
	searchSelection: EventEmitter<any> = new EventEmitter<any>();

	public searchResults: any[];
	public selection: any;
	public searchText: string;
	public searchTextChangeSubject: Subject<void> = new Subject<void>();

	constructor(private formBuilder: FormBuilder) {}

	public searchBarForm = this.formBuilder.group({
		selectedLocation: [null],
	});

	ngOnInit(): void {
		this.searchTextChangeSubject
			.pipe(
				debounceTime(500),
				switchMap(() => this.searchCallback(this.searchText))
			)
			.subscribe((results) => (this.searchResults = results));
	}

	searchTextChanged() {
		this.selection = null;
		this.searchTextChangeSubject.next();
	}

	displayFn(location: any): string {
		return location && location.display_name ? location.display_name : '';
	}

	onSelectionChange(event: any) {
		this.selection = event.source.value;
		this.searchSelection.emit(this.selection);
	}

	onSearchButtonPress() {
		if (this.selection) {
			this.searchSelection.emit(this.selection);
		}
	}
}
