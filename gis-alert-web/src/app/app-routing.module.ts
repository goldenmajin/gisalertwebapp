import { UsersStatisticResolver } from './statistics/resolve/user-statistic.resolve';
import { EventStatisticResolver } from './statistics/resolve/event-statistic.resolve';
import { StatisticsDashboardComponent } from './statistics/statistics-dashboard.component';
import { SecurityComponent } from './security/security.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/auth/auth.guard';
import { RolesResolver } from './security/resolve/roles.resolve';
import { RegisterGuard } from 'src/auth/register.guard';
const routes: Routes = [
	{
		path: '',
		canActivateChild: [AuthGuard],
		children: [
			{
				path: '',
				loadChildren: () =>
					import('./event-management/event-management.module').then(
						(m) => m.EventManagementModule
					),
			},
		],
	},
	{
		path: 'login',
		component: SecurityComponent,
	},
	{
		canActivate: [RegisterGuard],
		path: 'register',
		component: SecurityComponent,
		resolve: {
			roles: RolesResolver,
		},
	},
	{
		path: 'statistics',
		component: StatisticsDashboardComponent,
		resolve: {
			eventStatistic: EventStatisticResolver,
			usersStatistic: UsersStatisticResolver,
		},
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
