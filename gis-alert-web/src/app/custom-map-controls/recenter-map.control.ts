import Control from 'ol/control/Control';
import View from 'ol/View';
import { fromLonLat } from 'ol/proj';

export class RecenterMapControl extends Control {
	constructor(opt_options: any) {
		const options = opt_options || {};

		const button = document.createElement('button');
		button.innerHTML = 'C';

		const element = document.createElement('div');
		element.className = 'recenter-map-control ol-unselectable ol-control';
		element.setAttribute('title', 'Recenter map');
		element.appendChild(button);

		super({
			element: element,
			target: options.target,
		});

		button.addEventListener('click', this.recenterMap.bind(this), false);
	}

	recenterMap() {
		const map = this.getMap();

		const layer = map
			?.getLayers()
			.getArray()
			.find((l) => l.getProperties()['name'] === 'markerLayer');

		if (!layer) return;

		const data = layer
			?.getLayersArray()[0]
			.getSource()
			.getFeatureById('markerFeature')
			.getProperties()['data'];

		map?.setView(
			new View({
				center: fromLonLat([data.longitude, data.latitude]),
				zoom: map.getView().getZoom() ?? 5,
			})
		);
	}
}
