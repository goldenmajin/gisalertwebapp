import { UsersStatisticResolver } from './resolve/user-statistic.resolve';
import { EventStatisticResolver } from './resolve/event-statistic.resolve';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './../material.module';
import { NgxEchartsModule } from 'ngx-echarts';
import { StatisticsDashboardComponent } from './statistics-dashboard.component';
import { CustomUiComponentsModule } from './../custom-ui-components/custom-ui-components.module';
import { NgModule } from '@angular/core';

@NgModule({
	declarations: [StatisticsDashboardComponent],
	imports: [
		CustomUiComponentsModule,
		NgxEchartsModule,
		MaterialModule,
		ReactiveFormsModule,
	],
	providers: [EventStatisticResolver, UsersStatisticResolver],
})
export class StatisticsModule {}
