import { StatisticsService } from '../../../api-services/statistics.service';
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class UsersStatisticResolver implements Resolve<Observable<any>> {
	constructor(private service: StatisticsService) {}
	resolve() {
		return this.service.getUsersStatistic();
	}
}
