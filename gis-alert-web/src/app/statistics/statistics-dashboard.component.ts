import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { EventService } from '@apiServices/event.service';
import { EChartsOption } from 'echarts';
import { StatisticsService } from '@apiServices/statistics.service';

@Component({
	selector: 'statistics',
	templateUrl: './statistics-dashboard.component.html',
	styleUrls: ['./statistics-dashboard.component.scss'],
})
export class StatisticsDashboardComponent implements OnInit {
	chartOption: EChartsOption;
	formGroup: FormGroup;
	adminCount: number;
	moderatorCount: number;
	isLoading = false;

	constructor(
		private service: StatisticsService,
		private formBuiler: FormBuilder,
		private activatedRoute: ActivatedRoute
	) {}

	public ngOnInit(): void {
		this.buildSearchForm();
		const eventStatistic = this.activatedRoute.snapshot.data['eventStatistic'];
		const usersStatistic = this.activatedRoute.snapshot.data['usersStatistic'];
		this.adminCount = usersStatistic.find(
			(x: any) => x.role == 'Administrator'
		).count;
		this.moderatorCount = usersStatistic.find(
			(x: any) => x.role == 'Moderator'
		).count;
		this.constructChart(eventStatistic);
	}

	buildSearchForm() {
		this.formGroup = this.formBuiler.group({
			dateFrom: [null],
			dateTo: [null],
		});
	}

	getEventTypeStatistic() {
		const searchData = this.formGroup.getRawValue();
		this.isLoading = true;
		this.service.getEventTypeStatistic(searchData).subscribe(
			(data) => {
				this.isLoading = false;
				this.constructChart(data);
			},
			() => {
				this.isLoading = false;
			}
		);
	}

	constructChart(data: any): void {
		const categoryNames = data.map((x: any) => x.name);
		const seriesData = data.map((x: any) => ({
			value: x.count,
			itemStyle: {
				color: `#${x.color}`,
			},
		}));
		this.chartOption = {
			textStyle: {
				color: '#fff',
			},
			xAxis: {
				type: 'category',
				data: categoryNames,
				name: 'Event type',
			},
			yAxis: {
				type: 'value',
			},
			series: [
				{
					data: seriesData,
					type: 'bar',
					label: {
						show: true,
						position: 'top',
						color: 'white',
						fontSize: 14,
					},
				},
			],
		};
	}
}
