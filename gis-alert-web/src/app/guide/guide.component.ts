import { Component } from '@angular/core';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faInstagram, faTwitter } from '@fortawesome/free-brands-svg-icons';

@Component({
	selector: 'guide',
	templateUrl: './guide.component.html',
	//styleUrls: ['event-overview.component.scss'],
})
export class GuideComponent {
	faTwitter = faTwitter as IconProp;
	faInstagram = faInstagram as IconProp;
}
