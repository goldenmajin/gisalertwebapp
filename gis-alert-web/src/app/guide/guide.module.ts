import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MaterialModule } from './../material.module';
import { GuideComponent } from './guide.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
	declarations: [GuideComponent],
	imports: [CommonModule, MaterialModule, FontAwesomeModule, RouterModule],
})
export class GuideModule {}
