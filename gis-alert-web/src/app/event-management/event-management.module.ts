import { EventTypeResolver } from './resolve/event-type.resolve';
import { EventDetailResolver } from './resolve/event-detail.resolve';
import { PipesModule } from './../../pipes/pipes.module';
import { CustomUiComponentsModule } from './../custom-ui-components/custom-ui-components.module';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MaterialModule } from './../material.module';
import { EventOverviewComponent } from './event-overview.component';
import { EventAddComponent } from './event-add.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EventManagementRoutingModule } from './event-management-routing.module';

@NgModule({
	declarations: [EventAddComponent, EventOverviewComponent],
	imports: [
		CommonModule,
		MaterialModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
		CustomUiComponentsModule,
		EventManagementRoutingModule,
		PipesModule,
		NgbModule,
	],
	providers: [EventDetailResolver, EventTypeResolver],
})
export class EventManagementModule {}
