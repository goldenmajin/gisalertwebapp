import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { UserFeedbackService } from './../../services/user-feedback.service';
import { DialogService } from './../../services/dialog.service';
import { MapLocation } from './../../types/map-location.type';
import { EventService } from './../../api-services/event.service';
import { MappingService } from './../../api-services/mapping.service';
import { Component, OnInit } from '@angular/core';
import Map from 'ol/Map';
import { toLonLat } from 'ol/proj';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { filter, merge, Subject, debounceTime } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { RecenterMapControl } from '../custom-map-controls/recenter-map.control';
import Feature from 'ol/Feature';

@Component({
	selector: 'app-event-add',
	templateUrl: './event-add.component.html',
	styleUrls: ['./event-add.component.scss'],
})
export class EventAddComponent implements OnInit {
	map: Map;
	mapRefresh$: Subject<MapLocation> = new Subject<MapLocation>();
	public formGroup: FormGroup;
	public eventDetail: any = null;
	public eventTypes: any = null;

	constructor(
		private eventService: EventService,
		private mappingService: MappingService,
		private formBuilder: FormBuilder,
		private dialogService: DialogService,
		private activatedRoute: ActivatedRoute,
		private userFeedbackService: UserFeedbackService,
		private router: Router
	) {}

	public locationSearchCallback = (searchText: string) => {
		return this.eventService.searchLocation(searchText);
	};

	public renderFeatures = (location: MapLocation) => {
		const layers = this.map.getLayers();
		this.map.removeLayer(layers.item(2));
		this.map.removeLayer(layers.item(1));

		var marker = this.mappingService.renderMarker({
			longitude: location.longitude,
			latitude: location.latitude,
			type: { color: this.formGroup.controls['type'].value.color },
		});

		marker.setId('markerFeature');

		this.map.addLayer(
			new VectorLayer({
				properties: { name: 'markerLayer' },
				source: new VectorSource({
					features: [marker],
				}),
				zIndex: 2,
			})
		);
	};

	public onMapClick = (evt: any) => {
		const coordinates = toLonLat(evt.coordinate);
		const location: MapLocation = {
			longitude: coordinates[0],
			latitude: coordinates[1],
		};

		this.setCoordinatesInForm(location);
		this.mapRefresh$.next(location);
	};

	ngOnInit(): void {
		this.eventDetail = this.activatedRoute.snapshot.data['event'];
		this.eventTypes = this.activatedRoute.snapshot.data['eventTypes'];
		this.initFormGroup();

		var coordinates = this.eventDetail
			? [this.eventDetail.longitude, this.eventDetail.latitude]
			: toLonLat([0, 0]);

		this.map = this.mappingService.createMap(
			coordinates[0],
			coordinates[1],
			5,
			'ol-add-map'
		);

		this.map.getControls().extend([new RecenterMapControl(null)]);

		this.mapRefresh$.subscribe((location: MapLocation) => {
			this.renderFeatures(location);
			this.mappingService.recenterMap(
				this.map,
				{ lat: location.latitude, lon: location.longitude },
				this.map.getView().getZoom() ?? 5
			);
			this.formGroup.controls['radius'].patchValue(null);
			this.formGroup.controls['radius'].markAsUntouched({ onlySelf: true });
		});

		this.map.on('click', this.onMapClick);
		this.initFormGroupEvents();

		if (this.eventDetail != null) {
			this.initLocationOnEdit();
		}
	}

	compareFn(type1: any, type2: any): boolean {
		return type1.id === type2.id;
	}

	initLocationOnEdit() {
		this.renderFeatures({
			longitude: this.eventDetail.longitude,
			latitude: this.eventDetail.latitude,
		});
		this.formGroup.controls['radius'].updateValueAndValidity({
			emitEvent: true,
		});
	}

	initFormGroup() {
		this.formGroup = this.formBuilder.group({
			title: [
				this.eventDetail ? this.eventDetail.title : null,
				[Validators.required, Validators.maxLength(30)],
			],
			latitude: [
				this.eventDetail ? this.eventDetail.latitude : null,
				[Validators.required, Validators.min(-85.06), Validators.max(85.06)],
			],
			longitude: [
				this.eventDetail ? this.eventDetail.longitude : null,
				[Validators.required, Validators.min(-180.0), Validators.max(180.0)],
			],
			radius: [
				this.eventDetail ? this.eventDetail.radius : null,
				[Validators.required, Validators.min(1)],
			],
			description: [
				this.eventDetail ? this.eventDetail.description : null,
				[Validators.required, Validators.maxLength(300)],
			],
			dateFrom: [
				this.eventDetail ? this.eventDetail.dateFrom : null,
				Validators.required,
			],
			dateTo: [
				this.eventDetail ? this.eventDetail.dateTo : null,
				Validators.required,
			],
			type: [
				this.eventDetail ? this.eventDetail.type : this.eventTypes[0],
				Validators.required,
			],
			files: new FormArray([]),
		});
	}

	initFormGroupEvents() {
		if (this.eventDetail === null) {
			this.formGroup.controls['radius'].disable({ emitEvent: false });
		}
		merge(
			this.formGroup.controls['longitude'].valueChanges,
			this.formGroup.controls['latitude'].valueChanges
		)
			.pipe(debounceTime(500))
			.subscribe(() => {
				if (
					this.formGroup.controls['longitude'].value &&
					this.formGroup.controls['latitude'].value &&
					this.isMarkerValid()
				) {
					this.formGroup.controls['radius'].enable({ emitEvent: false });
					const location = this.getMapLocation();
					this.mapRefresh$.next(location);
				} else {
					this.formGroup.controls['radius'].patchValue(null);
					this.formGroup.controls['radius'].disable();
				}
			});

		this.formGroup.controls['radius'].valueChanges
			.pipe(filter((value) => !!value))
			.subscribe((value) => this.refreshMarkerOnRadiusChange(value));

		this.formGroup.controls['type'].valueChanges
			.pipe(filter((value) => !!value && this.isMarkerValid()))
			.subscribe(() => this.refreshMarkerOnTypeChange());
	}

	isMarkerValid(): boolean {
		return (
			this.formGroup.controls['longitude'].valid &&
			this.formGroup.controls['latitude'].valid
		);
	}

	getMapLocation(): MapLocation {
		return {
			longitude: this.formGroup.controls['longitude'].value,
			latitude: this.formGroup.controls['latitude'].value,
		};
	}

	getCircularArea(value: number | null): Feature<any> {
		return this.mappingService.renderCircularArea(
			{
				lon: this.formGroup.controls['longitude'].value,
				lat: this.formGroup.controls['latitude'].value,
				radius: value ?? this.formGroup.controls['radius'].value,
				color: this.formGroup.controls['type'].value.color,
			},
			this.mappingService.getResolutionFactor(this.map)
		);
	}

	refreshMarkerOnRadiusChange(value: number) {
		var layers = this.map.getLayers();
		this.map.removeLayer(layers.item(2));

		var circleArea = this.getCircularArea(value);

		this.map.addLayer(
			new VectorLayer({
				source: new VectorSource({
					features: [circleArea],
				}),
				zIndex: 1,
			})
		);
	}

	refreshMarkerOnTypeChange() {
		this.mappingService.resetMap(this.map);
		this.renderFeatures(this.getMapLocation());
		var circleArea = this.getCircularArea(null);

		this.map.addLayer(
			new VectorLayer({
				source: new VectorSource({
					features: [circleArea],
				}),
				zIndex: 1,
			})
		);
	}

	onSearchSelection(event: any) {
		const location: MapLocation = {
			latitude: +event.lat,
			longitude: +event.lon,
		};

		this.setCoordinatesInForm(location);
		this.mapRefresh$.next(location);
	}

	setCoordinatesInForm(location: MapLocation) {
		this.formGroup.controls['longitude'].setValue(
			location.longitude.toFixed(7),
			{ emitEvent: false }
		);
		this.formGroup.controls['latitude'].setValue(location.latitude.toFixed(7), {
			emitEvent: false,
		});
		this.formGroup.controls['radius'].enable();
	}

	openImageDialog(event: string) {
		this.dialogService.openImageDialog([event]);
	}

	mapEventSet(detail: any): any {
		return {
			title: detail.title,
			longitude: detail.longitude,
			latitude: detail.latitude,
			radius: detail.radius,
			description: detail.description,
			dateFrom: detail.dateFrom,
			dateTo: detail.dateTo,
			typeId: detail.type.id,
		};
	}

	save() {
		if (this.formGroup.valid) {
			var detail = this.formGroup.getRawValue();

			let saveAction: Observable<any>;

			if (this.eventDetail) {
				saveAction = this.eventService.edit(
					this.eventDetail.id,
					this.mapEventSet(detail)
				);
			} else {
				const addEvent = { ...detail, typeId: detail.type.id };
				delete addEvent.type;
				saveAction = this.eventService.add(addEvent);
			}

			saveAction.subscribe({
				next: () => {
					this.router.navigate(['/'], {
						queryParams: { lat: detail.latitude, lon: detail.longitude },
					});
				},
				error: (error) => {
					this.userFeedbackService.showErrorToast(error.error.message);
				},
			});
		}
	}
}
