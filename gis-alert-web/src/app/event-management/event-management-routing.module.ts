import { EventDetailResolver } from './resolve/event-detail.resolve';
import { EventAddComponent } from './event-add.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventOverviewComponent } from './event-overview.component';
import { EventTypeResolver } from './resolve/event-type.resolve';

const routes: Routes = [
	{
		path: '',
		component: EventOverviewComponent,
		pathMatch: 'full',
	},
	{
		path: 'add',
		component: EventAddComponent,
		resolve: {
			eventTypes: EventTypeResolver,
		},
	},
	{
		path: 'event/:id',
		component: EventAddComponent,
		resolve: {
			event: EventDetailResolver,
			eventTypes: EventTypeResolver,
		},
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class EventManagementRoutingModule {}
