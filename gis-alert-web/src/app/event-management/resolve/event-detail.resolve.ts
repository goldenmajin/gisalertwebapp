import { EventService } from './../../../api-services/event.service';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class EventDetailResolver implements Resolve<Observable<any>> {
	constructor(private eventService: EventService) {}
	resolve(route: ActivatedRouteSnapshot) {
		return this.eventService.get(route.params['id']);
	}
}
