import { EventService } from '../../../api-services/event.service';
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class EventTypeResolver implements Resolve<Observable<any>> {
	constructor(private eventService: EventService) {}
	resolve() {
		return this.eventService.getEventTypes();
	}
}
