import { Roles } from './../security/enums/role.enum';
import { UserService } from '@apiServices/user.service';
import { MapLocation } from './../../types/map-location.type';
import { DialogService } from './../../services/dialog.service';
import { EventService } from '../../api-services/event.service';
import {
	AfterViewInit,
	Component,
	ElementRef,
	OnInit,
	ViewChild,
} from '@angular/core';
import Map from 'ol/Map';
import { MappingService } from '@apiServices/mapping.service';
import { Subject, switchMap, takeUntil } from 'rxjs';
import Overlay from 'ol/Overlay';
import { Router, ActivatedRoute } from '@angular/router';
import { fromLonLat } from 'ol/proj';

@Component({
	selector: 'event-overview',
	templateUrl: './event-overview.component.html',
	styleUrls: ['event-overview.component.scss'],
})
export class EventOverviewComponent implements OnInit, AfterViewInit {
	@ViewChild('popup') popupScreen: ElementRef;
	map: Map;
	locations: any;
	searchResult: any;
	searchText: string;
	popup: Overlay;
	popupData: any;
	queryParamsLocation: MapLocation;
	userRole: string;
	roles = Roles;

	private $destroy = new Subject();
	private $update = new Subject();

	constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private mappingService: MappingService,
		private eventService: EventService,
		private dialogService: DialogService,
		private userService: UserService
	) {
		this.userRole = this.userService.getCurrentUserRole();
	}

	ngOnInit(): void {
		const lon = this.activatedRoute.snapshot.queryParamMap.get('lon');
		const lat = this.activatedRoute.snapshot.queryParamMap.get('lat');

		if (lon && lat) {
			this.queryParamsLocation = {
				longitude: +lon,
				latitude: +lat,
			};
		}

		this.$update
			.pipe(switchMap(() => this.eventService.getAll()))
			.subscribe((result) => {
				this.locations = result;
				this.mappingService.resetMap(this.map);
				this.mappingService.renderFeatures(this.map, this.locations);
			});
	}

	renderComplete = (evt: any) =>
		setTimeout(() => {
			if (!this.queryParamsLocation) return;
			const pixel = this.map.getPixelFromCoordinate(
				fromLonLat([
					this.queryParamsLocation.longitude,
					this.queryParamsLocation.latitude,
				])
			);
			const featureAtPixel = this.map.getFeaturesAtPixel(pixel)[0];
			if (featureAtPixel && featureAtPixel.get('type') === 'marker') {
				this.popupData = featureAtPixel.get('data');
				this.popup.setPosition(
					fromLonLat([
						this.queryParamsLocation.longitude,
						this.queryParamsLocation.latitude,
					])
				);
			}
		}, 500);

	ngAfterViewInit() {
		if (this.queryParamsLocation) {
			this.initializeMap(
				this.queryParamsLocation.longitude,
				this.queryParamsLocation.latitude,
				10
			);
			this.map.once('rendercomplete', this.renderComplete);
		} else {
			navigator.geolocation.getCurrentPosition(
				(position) => {
					this.initializeMap(
						position.coords.longitude,
						position.coords.latitude,
						8
					);
				},
				() => {
					this.initializeMap(24.6859225, 45.9852129, 8);
				}
			);
		}
	}

	initializeMap(longitude: number, latitude: number, zoom: number) {
		this.map = this.mappingService.createMap(
			longitude,
			latitude,
			zoom,
			'ol-map'
		);
		this.$update.next(null);
		this.addOverlay();
	}

	addOverlay() {
		this.popup = new Overlay({
			element: this.popupScreen.nativeElement,
			id: 'popup',
			positioning: 'center-center',
		});

		this.map.addOverlay(this.popup);

		const popup = this.popup;
		let popupDataSet = (data: any) => {
			this.popupData = data;
		};

		this.map.on('click', function (evt) {
			evt.map.forEachFeatureAtPixel(evt.pixel, (feature) => {
				if (feature.get('type') === 'marker') {
					const coordinate = evt.coordinate;
					popupDataSet(feature.get('data'));
					popup.setPosition(coordinate);
				}
			});
		});
	}

	deleteEvent(id: number) {
		this.eventService.delete(id).subscribe(() => {
			this.$update.next(null);
			this.closePopup();
		});
	}

	closePopup() {
		this.popup.setPosition(undefined);
	}

	recenterMap(location: any) {
		this.mappingService.recenterMap(this.map, location, 15);
	}

	searchLocation() {
		this.eventService
			.searchLocation(this.searchText)
			.pipe(takeUntil(this.$destroy))
			.subscribe((result) => (this.searchResult = result));
	}

	openImageDialog() {
		this.dialogService.openImageDialog(this.popupData?.imagePaths);
	}

	clearInput() {
		this.searchText = '';
	}

	logout() {
		localStorage.removeItem('token');
		this.router.navigateByUrl('/login');
	}

	ngOnDestroy() {
		this.$destroy.next(null);
		this.$destroy.complete();
	}
}
