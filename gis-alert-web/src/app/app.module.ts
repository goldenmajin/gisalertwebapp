import { StatisticsService } from './../api-services/statistics.service';
import { StatisticsModule } from './statistics/statistics.module';
import { RolesResolver } from './security/resolve/roles.resolve';
import { UserFeedbackService } from './../services/user-feedback.service';
import { DialogService } from './../services/dialog.service';
import { GuideModule } from './guide/guide.module';
import { SecurityModule } from './security/security.module';
import { EventManagementModule } from './event-management/event-management.module';
import { UserService } from '@apiServices/user.service';
import { EventService } from '../api-services/event.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MappingService } from '@apiServices/mapping.service';
import { AuthInterceptor } from 'src/auth/auth.interceptor';
import { NgxEchartsModule } from 'ngx-echarts';

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		HttpClientModule,
		EventManagementModule,
		SecurityModule,
		GuideModule,
		StatisticsModule,
		NgxEchartsModule.forRoot({
			echarts: () => import('echarts'),
		}),
	],
	providers: [
		UserFeedbackService,
		DialogService,
		MappingService,
		EventService,
		UserService,
		StatisticsService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptor,
			multi: true,
		},
		RolesResolver,
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
