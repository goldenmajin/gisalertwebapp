import { CustomUiComponentsModule } from './../custom-ui-components/custom-ui-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { MaterialModule } from './../material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SecurityComponent } from './security.component';

@NgModule({
	declarations: [SecurityComponent, LoginComponent, RegisterComponent],
	imports: [
		CommonModule,
		MaterialModule,
		FormsModule,
		ReactiveFormsModule,
		CustomUiComponentsModule,
	],
})
export class SecurityModule {}
