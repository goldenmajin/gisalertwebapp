import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { UserService } from '@apiServices/user.service';

@Injectable()
export class RolesResolver implements Resolve<Observable<any>> {
	constructor(private userService: UserService) {}
	resolve() {
		return this.userService.getRoles();
	}
}
