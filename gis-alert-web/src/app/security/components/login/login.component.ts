import { UserFeedbackService } from './../../../../services/user-feedback.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '@apiServices/user.service';
import { LoginModel } from '@apiTypes/login-model.type';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
	constructor(
		private formBuilder: FormBuilder,
		private service: UserService,
		private router: Router,
		private userFeedbackService: UserFeedbackService
	) {}

	public formGroup = this.formBuilder.group({
		userName: [null, [Validators.required, Validators.maxLength(256)]],
		password: [null, [Validators.minLength(6), Validators.required]],
	});

	ngOnInit() {
		if (localStorage.getItem('token') != null) {
			this.router.navigateByUrl('/');
		}
	}

	login() {
		if (!this.formGroup.valid) {
			return;
		}
		const value = this.formGroup.getRawValue();
		const model: LoginModel = {
			userName: value.userName,
			password: value.password,
		};

		this.service.login(model).subscribe(
			(res: any) => {
				localStorage.setItem('token', res.token);
				this.router.navigateByUrl('/');
			},
			(err) => this.userFeedbackService.showErrorToast(err.error.message)
		);
	}
}
