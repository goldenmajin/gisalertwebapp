import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RegistrationModel } from '@apiTypes/registration-model.type';
import { UserService } from '@apiServices/user.service';
import { UserFeedbackService } from 'src/services/user-feedback.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
	@Input()
	roles: any[];

	constructor(
		private formBuilder: FormBuilder,
		private service: UserService,
		private feedbackService: UserFeedbackService,
		private router: Router
	) {}

	public formGroup = this.formBuilder.group({
		userName: [null, [Validators.required, Validators.maxLength(256)]],
		email: [null, [Validators.email, Validators.required]],
		password: [null, [Validators.minLength(6), Validators.required]],
		roleId: [null, [Validators.required]],
	});

	ngOnInit() {}

	onSubmit() {
		if (!this.formGroup.valid) {
			return;
		}
		const value = this.formGroup.getRawValue();
		const model: RegistrationModel = {
			userName: value.userName,
			email: value.email,
			password: value.password,
			roleId: value.roleId,
		};

		this.service.register(model).subscribe(
			() => {
				this.router.navigate(['/']);
			},
			(errorResponse) =>
				this.feedbackService.showErrorToast(errorResponse.error.errors)
		);
	}
}
