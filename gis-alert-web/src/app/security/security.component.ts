import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-security',
	templateUrl: './security.component.html',
	styleUrls: ['./security.component.scss'],
})
export class SecurityComponent implements OnInit {
	public roles: any[];

	constructor(private activatedRoute: ActivatedRoute) {}

	ngOnInit() {
		this.roles = this.activatedRoute.snapshot.data['roles'];
	}
}
