import { ImageDialogComponent } from '../app/custom-ui-components/image-dialog/image-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root',
})
export class DialogService {
	constructor(private matDialog: MatDialog) {}

	openImageDialog(images: string[]) {
		this.matDialog.open(ImageDialogComponent, {
			width: '900px',
			data: {
				images: images,
			},
		});
	}
}
