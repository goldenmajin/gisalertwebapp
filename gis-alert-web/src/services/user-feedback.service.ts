import { Injectable } from '@angular/core';
import {
	MatSnackBar,
	MatSnackBarHorizontalPosition,
	MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

@Injectable({
	providedIn: 'root',
})
export class UserFeedbackService {
	horizontalPosition: MatSnackBarHorizontalPosition = 'end';
	verticalPosition: MatSnackBarVerticalPosition = 'top';
	constructor(private snackBar: MatSnackBar) {}

	showErrorToast(errorMessage: string) {
		this.snackBar.open(errorMessage, 'Close', {
			horizontalPosition: this.horizontalPosition,
			verticalPosition: this.verticalPosition,
			panelClass: ['error-toast'],
		});
	}
}
