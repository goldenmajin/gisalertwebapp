import { UserService } from '@apiServices/user.service';
import {
	ActivatedRouteSnapshot,
	CanActivate,
	Router,
	RouterStateSnapshot,
} from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root',
})
export class RegisterGuard implements CanActivate {
	constructor(private router: Router, private userService: UserService) {}

	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): boolean {
		var role = this.userService.getCurrentUserRole();
		if (role && role === 'Administrator') {
			return true;
		} else {
			this.router.navigate(['/']);
			return false;
		}
	}
}
