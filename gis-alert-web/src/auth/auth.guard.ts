import {
	ActivatedRouteSnapshot,
	CanActivate,
	CanActivateChild,
	CanLoad,
	Router,
	RouterStateSnapshot,
} from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanLoad, CanActivateChild {
	constructor(private router: Router) {}

	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): boolean {
		return this.checkTokenExists();
	}

	canLoad() {
		return this.checkTokenExists();
	}

	canActivateChild() {
		return this.checkTokenExists();
	}

	private checkTokenExists() {
		if (localStorage.getItem('token') != null) return true;
		else {
			this.router.navigate(['login']);
			return false;
		}
	}
}
